from app.models import *
from csv import DictReader


def create(nome):
    new_wizard = Wizard(nome, 100, 100)

    new_wizard.create_id()

    new_wizard.save('wizards.csv')

    keys = ['id', 'name', 'health_pool', 'energy_pool']
    values = [new_wizard.id, new_wizard.nome, new_wizard.health_pool, new_wizard.energy_pool]

    new_dict = zip(keys, values)

    return dict(new_dict)


def list_all(filename):

    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        wizard_list = list(reader)

    return wizard_list


def expel_wizard(filename, wizard_id):

    with open(filename, 'r') as f:
        wizard_file = f.readlines()

    if len(wizard_file) > int(wizard_id):
        with open(filename, 'w') as f:
            wizard_file.pop(int(wizard_id))
            f.writelines(wizard_file)
