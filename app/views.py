from flask import Blueprint, Flask, render_template, request, jsonify
from app.services import *
from json import dumps

bp_home = Blueprint('home_blueprint', __name__)
bp_enroll = Blueprint('enroll_blueprint', __name__)
bp_wizards = Blueprint('wizards_blueprint', __name__)
bp_expel = Blueprint('expel_blueprint', __name__)


@bp_home.route('/')
def home():
    return render_template('index.html')


@bp_enroll.route('/enroll', methods=["POST"])
def enroll():
    new_dict = create(request.json['nome'])

    return dumps(new_dict)


@bp_wizards.route("/wizards",  methods=["GET"])
def wizards():
    new_dict = list_all('wizards.csv')
    
    return dumps(new_dict)


@bp_expel.route("/expel/<wizard_id>", methods=["DELETE"])
def expel(wizard_id):

    expel_wizard('wizards.csv', wizard_id)

    return 'Wizard removido com sucesso.'
