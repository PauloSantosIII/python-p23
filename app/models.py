import csv

def get_id(filename):
    id = 1
    with open (filename, 'r') as id_csv:
        reader = csv.reader(id_csv)
        header = next(reader)
        for line in reader:
            id += int(1)

        return id

class Wizard:
    
    def __init__(self, nome, health_pool, energy_pool):
        self.nome = nome
        self.health_pool = 100
        self.energy_pool = 100
        self.id = 0
    
    def create_id(self):
        # deve retornar o próximo id único
        self.id += get_id('wizards.csv')

        return self.id

    def save(self, filename):
        # deve adicionar o bruxo no arquivo CSV com nome filename
        with open(filename, 'a') as f:
            writer = csv.writer(f, delimiter=',', lineterminator='\n')
            new_line = [self.id, self.nome, self.health_pool, self.energy_pool]
            writer.writerow(new_line)

            f.close()
