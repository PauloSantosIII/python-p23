from flask import Flask, Blueprint
from app.views import *

def create_app():
    app = Flask(__name__)

    app.register_blueprint(bp_home)
    app.register_blueprint(bp_enroll)
    app.register_blueprint(bp_wizards)
    app.register_blueprint(bp_expel)

    return app